package logic;

import java.io.FileReader;
import java.time.LocalDateTime;
import java.util.Iterator;

import com.opencsv.CSVReader;

import vo.VOBike;
import vo.VOStation;
import api.IManager;
import data_structures.List;
import data_structures.RedBlackBST;
import view.View;
import vo.VOTrip;

public class Manager implements IManager {

	public final static String TRIPS_FILE_Q1 = "./data/Divvy_Trips_2017_Q1.csv";
	public final static String TRIPS_FILE_Q2 = "./data/Divvy_Trips_2017_Q2.csv";
	public final static String TRIPS_FILE_Q3 = "./data/Divvy_Trips_2017_Q3.csv";
	public final static String TRIPS_FILE_Q4 = "./data/Divvy_Trips_2017_Q4.csv";
	public final static String STATIONS_FILEQ1_Q2 = "./data/Divvy_Stations_2017_Q1Q2.csv";
	public final static String STATIONS_FILEQ3_Q4 = "./data/Divvy_Stations_2017_Q3Q4.csv";

	RedBlackBST<Integer, VOStation> stationsTree = new RedBlackBST<>();
	//RedBlackBST<Integer, VOTrip> tripsTree = new RedBlackBST<>();
	List<VOTrip> tripsTree = new List<>();
	RedBlackBST<Integer, VOBike> bikesTree = new RedBlackBST<>();

	@Override
	public void load() {
		try {

			//Cargar Estaciones
			System.out.println("Cargando estaciones...");
			CSVReader reader = new CSVReader(new FileReader(STATIONS_FILEQ3_Q4));
			String[] nextLine;
			reader.readNext();

			while ((nextLine = reader.readNext()) != null) {
				LocalDateTime time = View.convertirFecha_Hora_LDT(nextLine[6].split(" ")[0], nextLine[6].split(" ")[1]);

				VOStation station = new VOStation(Integer.parseInt(nextLine[0]), nextLine[1], nextLine[2],
						Double.parseDouble(nextLine[3]), Double.parseDouble(nextLine[4]),
						Integer.parseInt(nextLine[5]), time);

				stationsTree.put(station.getId(), station);
			}

			System.out.println(stationsTree.size());
			reader.close();

			//Cargar Viajes
			System.out.println("Cargando Q1...");
			reader = new CSVReader(new FileReader(TRIPS_FILE_Q1));
			reader.readNext();

			while((nextLine = reader.readNext()) != null) {
				VOTrip trip = new VOTrip(Integer.parseInt(nextLine[0]),
						Integer.parseInt(nextLine[3]), Integer.parseInt(nextLine[4]), 
						Integer.parseInt(nextLine[5]), nextLine[6], Integer.parseInt(nextLine[7]), 
						nextLine[8]);
				inicializarDistancias(trip);
				
				tripsTree.add(trip);
			}

			System.out.println("Cargando Q2...");
			reader = new CSVReader(new FileReader(TRIPS_FILE_Q2));
			reader.readNext();
			while((nextLine = reader.readNext()) != null) {
				VOTrip trip = new VOTrip(Integer.parseInt(nextLine[0]),
						Integer.parseInt(nextLine[3]), Integer.parseInt(nextLine[4]), 
						Integer.parseInt(nextLine[5]), nextLine[6], Integer.parseInt(nextLine[7]), 
						nextLine[8]);
				inicializarDistancias(trip);
				
				tripsTree.add(trip);
			}

			System.out.println("Cargando Q3...");
			reader = new CSVReader(new FileReader(TRIPS_FILE_Q3));
			reader.readNext();
			while((nextLine = reader.readNext()) != null) {
				VOTrip trip = new VOTrip(Integer.parseInt(nextLine[0]),
						Integer.parseInt(nextLine[3]), Integer.parseInt(nextLine[4]), 
						Integer.parseInt(nextLine[5]), nextLine[6], Integer.parseInt(nextLine[7]), 
						nextLine[8]);
				inicializarDistancias(trip);
				
				tripsTree.add(trip);
			}

			System.out.println("Cargando Q4...");
			reader = new CSVReader(new FileReader(TRIPS_FILE_Q4));
			reader.readNext();
			while((nextLine = reader.readNext()) != null) {
				VOTrip trip = new VOTrip(Integer.parseInt(nextLine[0]),
						Integer.parseInt(nextLine[3]), Integer.parseInt(nextLine[4]), 
						Integer.parseInt(nextLine[5]), nextLine[6], Integer.parseInt(nextLine[7]), 
						nextLine[8]);
				inicializarDistancias(trip);
				
				tripsTree.add(trip);
			}
			
			cargarBicicletas();
			
			/*Iterator<Integer> iter = bikesTree.iterator();
			while(iter.hasNext()) {
				int id = iter.next();
				System.out.println(id);
				break;
			}*/

			System.out.println("Se cargaron " + bikesTree.size() + " bicicletas.");

			System.out.println("Id min: " + bikesTree.min());
			System.out.println("Id max: " + bikesTree.max());

		} catch(Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}

	private void inicializarDistancias(VOTrip trip) {
		VOStation fromStation = stationsTree.get(trip.getFromStationId());
		VOStation toStation = stationsTree.get(trip.getToStationId());

		trip.setFromStationLatitud(fromStation.getLatitude());
		trip.setFromStationLongitud(fromStation.getLongitude());

		trip.setToStationLatitud(toStation.getLatitude());
		trip.setToStationLongitud(toStation.getLongitude());
	}
	
	private void cargarBicicletas() {
		Iterator<VOTrip> iter = tripsTree.iterator();
		
		while(iter.hasNext()) {
			VOTrip trip = iter.next();
			VOBike bike = bikesTree.get(trip.getBikeId());
			if(bike != null) {
				bike.addTrip();
				double fromLong = stationsTree.get(trip.getFromStationId()).getLongitude();
				double toLong = stationsTree.get(trip.getToStationId()).getLongitude();
				double fromLat = stationsTree.get(trip.getFromStationId()).getLatitude();
				double toLat = stationsTree.get(trip.getToStationId()).getLatitude();
				double distance = Haversine.distance(fromLat, fromLong, toLat, toLong);

				bike.addDistance(distance);
				bike.addTime(trip.getTripDuration());
				
			} else {
				bike = new VOBike(trip.getBikeId(), trip.getDistance(), trip.getTripDuration(), 1);
			}
			
			bikesTree.put(bike.getId(), bike);
			
		}
	}

	@Override
	public VOBike consultarBicicleta(int id)
	{
		return bikesTree.get(id);
	}	

	@Override
	public List<Integer> consultarIds(int idMenor, int idMayor) {
		List<Integer> ids = new List<>();
		Iterator<Integer> iter = bikesTree.keysInRange(idMenor, idMayor);

		while(iter.hasNext()) {
			int id = iter.next();

			ids.add(id);
		}

		return ids;
	}

}
