package vo;

public class VOBike implements Comparable<VOBike> {

	private int id;
	
	private double distance;
	
	private double time;
	
	private double numTrips;

	/**
	 * @param id
	 * @param distance
	 * @param time
	 * @param numTrips
	 */
	public VOBike(int id, double distance, double time, double numTrips) {
		this.id = id;
		this.distance = distance;
		this.time = time;
		this.numTrips = numTrips;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @return the distance
	 */
	public double getDistance() {
		return distance;
	}
	
	public void addDistance(double distance) {
		this.distance += distance;
	}

	/**
	 * @return the time
	 */
	public double getTime() {
		return time;
	}
	
	public void addTime(double time) {
		this.time += time;
	}

	/**
	 * @return the numTrips
	 */
	public double getNumTrips() {
		return numTrips;
	}
	
	public void addTrip() {
		numTrips ++;
	}

	@Override
	public int compareTo(VOBike o) {
		// TODO Auto-generated method stub
		return 0;
	}
	
}
