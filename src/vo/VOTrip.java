package vo;

import java.time.LocalDateTime;

import logic.Haversine;

/**
 * Representation of a Trip object
 */
public class VOTrip implements Comparable<VOTrip>{
	//-----------------------------------------------------------
	// Atributos
	//-----------------------------------------------------------

	private int id;

	private int bikeId;

	private int tripDuration;

	private int fromStationId;

	private String fromStationName;

	private int toStationId;

	private String toStationName;

	private int numTrips;

	private double distance;

	private double time;

	private double fromStationLatitud;

	private double fromStationLongitud;

	private double toStationLatitud;

	private double toStationLongitud;


	//-----------------------------------------------------------
	// Constructor
	//-----------------------------------------------------------

	/**
	 * @param id
	 * @param startTime
	 * @param endTime
	 * @param bikeId
	 * @param tripDuration
	 * @param fromStationId
	 * @param fromStationName
	 * @param toStationId
	 * @param toStationName
	 * @param userType
	 * @param gender
	 * @param birthYear
	 */
	public VOTrip(int id, int bikeId, int tripDuration, int fromStationId, String fromStationName, int toStationId, String toStationName) {
		this.id = id;
		this.bikeId = bikeId;
		this.tripDuration = tripDuration;
		this.fromStationId = fromStationId;
		this.fromStationName = fromStationName;
		this.toStationId = toStationId;
		this.toStationName = toStationName;
		numTrips = 0;
		distance = 0;
		time = 0;
		fromStationLatitud = 0;
		fromStationLongitud = 0;
		toStationLatitud = 0;
		toStationLongitud = 0;
	}

	//-----------------------------------------------------------
	// Atributos
	//-----------------------------------------------------------

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @return the bikeId
	 */
	public int getBikeId() {
		return bikeId;
	}

	/**
	 * @return the tripDuration
	 */
	public int getTripDuration() {
		return tripDuration;
	}

	/**
	 * @return the fromStationId
	 */
	public int getFromStationId() {
		return fromStationId;
	}

	/**
	 * @return the fromStationName
	 */
	public String getFromStationName() {
		return fromStationName;
	}

	/**
	 * @return the toStationId
	 */
	public int getToStationId() {
		return toStationId;
	}

	/**
	 * @return the toStationName
	 */
	public String getToStationName() {
		return toStationName;
	}

	/**
	 * @return the numTrips
	 */
	public int getNumTrips() {
		return numTrips;
	}

	public void addTrip() {
		numTrips ++;
	}

	/**
	 * @return the distance
	 */
	public double getDistance() {
		return distance;
	}

	public void addDistance(double distance) {
		this.distance += distance;
	}

	/**
	 * @return the time
	 */
	public double getTime() {
		return time;
	}

	public void addTime(double time) {
		this.time += time;
	}
	
	public double getFromStationLatitud() {
		return fromStationLatitud;
	}

	public void setFromStationLatitud(double fromStationLatitud) {
		this.fromStationLatitud = fromStationLatitud;
	}

	public double getFromStationLongitud() {
		return fromStationLongitud;
	}

	public void setFromStationLongitud(double fromStationLongitud) {
		this.fromStationLongitud = fromStationLongitud;
	}

	public double getToStationLatitud() {
		return toStationLatitud;
	}

	public void setToStationLatitud(double toStationLatitud) {
		this.toStationLatitud = toStationLatitud;
	}

	public double getToStationLongitud() {
		return toStationLongitud;
	}

	public void setToStationLongitud(double toStationLongitud) {
		this.toStationLongitud = toStationLongitud;
	}
	
	public double darDistancia()
	{
		return Haversine.distance(fromStationLatitud, fromStationLongitud, toStationLatitud, toStationLongitud);
		
	}

	@Override
	public int compareTo(VOTrip o) {
		if(fromStationName.compareTo(o.getFromStationName()) < 0) {
			if(toStationId == o.getToStationId()) {

				if(tripDuration < o.getTripDuration()) {
					return -1;
				} else if(tripDuration > o.getTripDuration()) {
					return 1;
				} else {
					return 0;
				}

			} else if(toStationName.compareTo(o.getToStationName()) > 0) {
				return -1;
			} else {
				return 1;
			}

		} else if(fromStationName.compareTo(o.fromStationName) < 0) {
			return -1;
		} else {
			return 1;
		}
	}
}
