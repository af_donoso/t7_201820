package data_structures;

import java.util.Iterator;

public interface IRedBlackBST<K extends Comparable<K>, V> {

	int size();
	boolean isEmpty();
	V get(K key);
	int getHeight(K key);
	int height();
	boolean contains(K key);
	void put(K key, V value);
	K min();
	K max();
	boolean check();
	Iterator<K> keys();
	Iterator<V> valuesInRange(K init, K end);
	Iterator<K> keysInRange(K init, K end);
	
}
