package data_structures;

public class Node<T> {
	//-----------------------------------------------------------
	// Attributes
	//-----------------------------------------------------------

	T element;

	Node<T> next;

	//-----------------------------------------------------------
	// Constructor
	//-----------------------------------------------------------

	/**
	 * @param element
	 * @param next
	 */
	public Node(T element) {
		this.element = element;
		this.next = null;
	}

	//-----------------------------------------------------------
	// Methods
	//-----------------------------------------------------------

	/**
	 * @return the element
	 */
	public T getElement() {
		return element;
	}

	/**
	 * @return the next
	 */
	public Node<T> getNext() {
		return next;
	}

	/**
	 * @param element the element to set
	 */
	public void setElement(T element) {
		this.element = element;
	}

	/**
	 * @param next the next to set
	 */
	public void setNext(Node<T> next) {
		this.next = next;
	}
}
