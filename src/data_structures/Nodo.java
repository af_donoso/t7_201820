package data_structures;

public class Nodo<K extends Comparable<K>, V> {

	//------------------------------------------------------
	//Atributos
	//------------------------------------------------------

	public final static String IZQUIERDA = "Izquierda";
	public final static String DERECHA = "Derecha";

	//------------------------------------------------------
	//Colores
	//------------------------------------------------------

	public enum Color {
		RED,
		BLACK
	}

	//------------------------------------------------------
	//Atributos
	//------------------------------------------------------

	Color color;
	K key;
	V value;
	Nodo<K,V> izquierda, derecha, padre;

	//------------------------------------------------------
	//Constructor
	//------------------------------------------------------

	public Nodo(K key, V value, Nodo<K,V> padre) {
		this.color = Color.RED;
		this.key = key;
		this.value = value;
		this.padre = padre;
		izquierda = null;
		derecha = null;
	}

	//------------------------------------------------------
	//Metodos
	//------------------------------------------------------

	public V darValor() {
		return value;
	}

	public Nodo<K,V> darHijoIzquierda() {
		return izquierda;
	}

	public Nodo<K,V> darHijoDerecha() {
		return derecha;
	}

	public Nodo<K,V> darPadre() {
		return padre;
	}

	/**
	 * Cambia el color del arco por el introducido por parametro.
	 * @param color Nuevo color.
	 */
	public void cambiarColor(Color color) {
		this.color = color;
	}

	/**
	 * Indica si el enlace es rojo. El enlace es con el padre del nodo.
	 * @param nodo Nodo a evaluar.
	 * @return True si es rojo y existe, false de lo contrario.
	 */
	public boolean esEnlanceRojo(Nodo<K,V> nodo) {
		return nodo != null && nodo.color == Color.RED;
	}

	public boolean esEnlaceRojo() {
		return this.color == Color.RED;
	}

	/**
	 * cambia los colores de los hijos del nodo a negro y el del nodo con su padre a rojo.
	 */
	private void flipColors() {
		izquierda.color = Color.BLACK;
		derecha.color = Color.BLACK;
		color = Color.RED;
	}

	private Nodo<K,V> rotateLeft() {
		Nodo<K,V> hermano = derecha;
		derecha = hermano.izquierda;
		hermano.izquierda = this;
		hermano.color = this.color;
		this.color = Color.RED;

		return hermano;
	}

	private Nodo<K,V> rotateRight() {
		Nodo<K,V> hermano = izquierda;
		izquierda = hermano.derecha;
		hermano.derecha = this;
		hermano.color = this.color;
		this.color = Color.RED;

		return hermano;
	}

	/**
	 * Inserta un nuevo nodo en el arbol. Si ya existe, cambia el valor asociado a la llave.
	 * @param key Llave del nodo.
	 * @param value Valor a guardar.
	 * @return Nodo creado.
	 */
	public Nodo<K,V> insertar(K key, V value) {
		if (this.key.compareTo(key) == 0) {
			this.value = value;
			return this;

		} else if (this.key.compareTo(key) > 0) {
			if (izquierda == null) {
				izquierda = new Nodo<K,V>(key, value, this);
			} else {
				izquierda = izquierda.insertar(key, value);
			}

		} else {
			if (derecha == null) {
				derecha = new Nodo<K,V>(key, value, this);
			} else {
				derecha = derecha.insertar(key, value);
			}
		}

		//Reparación de sub-arbol
		Nodo<K,V> h = this;
		if (esEnlanceRojo(h.derecha) && !esEnlanceRojo(h.izquierda)) h = h.rotateLeft();
		if (esEnlanceRojo(h.izquierda) && esEnlanceRojo(h.izquierda.izquierda)) h = h.rotateRight();
		if (esEnlanceRojo(h.izquierda) && esEnlanceRojo(h.derecha)) h.flipColors();

		return h;
	}

	/**
	 * Busca el nodo con la llave introducida por parametro.
	 * @param key Llave del nodo.
	 * @return Nodo encontrado, null si no se encontro.
	 */
	public Nodo<K,V> buscar(K key) {
		Nodo<K,V> nodo = null;

		if (this.key.compareTo(key) == 0) {
			nodo = this;
		} else if (this.izquierda != null && this.key.compareTo(key) > 0) {
			nodo = izquierda.buscar(key);
		} else if (this.derecha != null) {
			nodo = derecha.buscar(key);
		}

		return nodo;
	}

	/**
	 * Busca el nodo más pequeño del sub-arbol.
	 * @return Nodo mas pequeño
	 */
	public Nodo<K,V> minimum() {
		if(this.izquierda == null) {
			return this;
		} else {
			return this.izquierda.minimum();
		}
	}

	/**
	 * Busca el nodo mas grande del sub-arbol
	 * @return Nodo mas grande
	 */
	public Nodo<K,V> maximum() {
		if(this.derecha == null) {
			return this;
		} else {
			return this.derecha.maximum();
		}
	}

	/**
	 * Retorna el peso del arbol.
	 * @return Peso del arbol.
	 */
	public int darPeso() {
		int peso = 1;

		if(izquierda != null) {
			peso += izquierda.darPeso();
		}

		if(derecha != null) {
			peso += derecha.darPeso();
		}

		return peso;
	}

	public int darAltura() {
		return darAltura(this);
	}

	/**
	 * Retorna la altura del arbol.
	 * @return altura del arbol.
	 */
	private int darAltura(Nodo<K,V> nodo) {
		if(nodo == null || esEnlanceRojo(nodo)) return -1;
		return 1 + Math.max(darAltura(nodo.derecha), darAltura(nodo.izquierda));
	}

	public int darAltura(K key) {
		Nodo<K, V> nodo = buscar(key);

		return darAltura(nodo);
	}

	public boolean esHoja() {
		return this.derecha == null && this.izquierda == null;
	}

	public boolean contains(K key) {
		boolean contains = false;

		if(this.key.compareTo(key) == 0) {
			contains = true;
		} else if(this.izquierda != null && this.key.compareTo(key) > 0) {
			contains = this.izquierda.contains(key);
		} else if(this.derecha != null) {
			contains = this.derecha.contains(key);
		}
		return contains;
	}

	//------------------------------------------------------
	//Métodos Check
	//------------------------------------------------------

	public boolean enlacesRojosDerecha() {
		boolean rojoDerecha = false;

		if(this.izquierda != null) {
			rojoDerecha = this.izquierda.enlacesRojosDerecha();
		}

		if(this.derecha != null) {
			if(this.derecha.esEnlaceRojo()) {
				rojoDerecha = true;
			} else {
				rojoDerecha = this.derecha.enlacesRojosDerecha();
			}
		}
		
		return rojoDerecha;
	}
	
	public boolean enlacesRojosIzquierdaConsecutivos() {
		boolean consecutivos = false;
		
		if(this.izquierda != null && this.izquierda.izquierda != null) {
			if(this.izquierda.esEnlaceRojo() && this.izquierda.izquierda.esEnlaceRojo()) {
				consecutivos = true;
			} else {
				consecutivos = this.izquierda.enlacesRojosIzquierdaConsecutivos();
			}
		}
		
		if(this.derecha != null) {
			consecutivos = this.derecha.enlacesRojosIzquierdaConsecutivos();
		}
		
		return consecutivos;
	}
	
	public boolean mismoNumeroEnlacesNegros() {
		int alturaIzquierda = 0;
		int alturaDerecha = 0;
		
		if(this.izquierda != null) {
			alturaIzquierda = this.izquierda.darAltura();
		}
		
		if(this.derecha != null) {
			alturaDerecha = this.derecha.darAltura();
		}
		
		return alturaIzquierda == alturaDerecha;
	}

}
