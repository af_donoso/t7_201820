package data_structures;

import java.util.Iterator;

import data_structures.Nodo.Color;
import data_structures.Queue;

public class RedBlackBST<K extends Comparable<K>, V> implements IRedBlackBST<K, V> {

	//------------------------------------------------------
	//Atributos
	//------------------------------------------------------

	private Nodo<K,V> raiz;

	//------------------------------------------------------
	//Constructores
	//------------------------------------------------------

	/**
	 * Crea un arbol rojo-negro vacio.
	 */
	public RedBlackBST() {
		raiz = null;
	}

	/**
	 * Crea un arbol rojo-negro con una raiz. 
	 * @param raiz
	 */
	public RedBlackBST(Nodo<K,V> raiz) {
		this.raiz = raiz;
	}

	//------------------------------------------------------
	//Metodos
	//------------------------------------------------------

	public Nodo<K,V> darRaiz() {
		return raiz;
	}

	@Override
	public void put(K key, V value) {
		if (raiz == null) {
			raiz = new Nodo<K,V>(key, value, null);
		} else {
			raiz = raiz.insertar(key, value);
		}

		raiz.cambiarColor(Color.BLACK);
	}

	public Nodo<K, V> buscarNodo(K key) {
		if(raiz == null) {
			return null;
		} else {
			return raiz.buscar(key);
		}
	}

	@Override
	public V get(K key) {
		if(raiz == null) {
			return null;
		} else {
			Nodo<K,V> nodo = raiz.buscar(key);
			if(nodo != null) {
				return nodo.value;
			} else {
				return null;
			}
		}
	}

	@Override
	public K min() {
		if(raiz == null) {
			return null;
		} else {
			return raiz.minimum().key;
		}
	}

	@Override
	public K max() {
		if(raiz == null) {
			return null;
		} else {
			return raiz.maximum().key;
		}
	}

	@Override
	public int size() {
		if(raiz == null) {
			return 0;
		} else {
			return raiz.darPeso();
		}
	}

	@Override
	public int height() {
		if(raiz == null) {
			return 0;
		} else {
			return raiz.darAltura();
		}
	}

	@Override
	public boolean isEmpty() {
		return raiz == null;
	}

	@Override
	public int getHeight(K key) {
		if(raiz == null) {
			return 0;
		} else {
			return raiz.darAltura(key);
		}
	}

	@Override
	public boolean contains(K key) {
		if(raiz == null) {
			return false;
		} else {
			return raiz.contains(key);
		}
	}

	@Override
	public boolean check() {
		/**
		 * Verificacion: Ordenado
		 */
		Iterator<K> iter = keys();
		K key1 = null, key2 = null;
		while(iter.hasNext()) {
			key1 = iter.next();

			if(key2 != null) {

				if(key1.compareTo(key2) < 0) {
					return false;
				}
			}

			key2 = key1;
		}

		/**
		 * Verificacion: No tener enlaces rojos con hijos a la derecha
		 */
		if(raiz != null) {
			if(raiz.enlacesRojosDerecha()) {
				System.out.println("ENLACES ROJOS DERECHA");
				return false;
			}
		}

		/**
		 * Verificacion: No tener dos enlaces rojos consecutivos a la izquierda.
		 */
		if(raiz != null) {
			if(raiz.enlacesRojosIzquierdaConsecutivos()) {
				System.out.println("ENLACES ROJOS CONSECUTIVOS");
				return false;
			}
		}

		/**
		 * Verificacion: Mismo numero de enlaces negros en cada rama.
		 */
		if(raiz != null) {
			if(!raiz.mismoNumeroEnlacesNegros()) {
				System.out.println("MISMO NUMERO ENLACES NEGROS");
				return false;
			}
		}

		return true;
	}
	
	public Iterator<K> keys() {
        if (isEmpty()) return new Queue<K>().iterator();
        return keys(min(), max());
    }
	
    public Iterator<K> keys(K lo, K hi) {
        if (lo == null) throw new IllegalArgumentException("first argument to keys() is null");
        if (hi == null) throw new IllegalArgumentException("second argument to keys() is null");

        Queue<K> queue = new Queue<K>();
        // if (isEmpty() || lo.compareTo(hi) > 0) return queue;
        keys(raiz, queue, lo, hi);
        return queue.iterator();
    } 

    private void keys(Nodo<K,V> x, Queue<K> queue, K lo, K hi) { 
        if (x == null) return; 
        int cmplo = lo.compareTo(x.key); 
        int cmphi = hi.compareTo(x.key); 
        if (cmplo < 0) keys(x.izquierda, queue, lo, hi); 
        if (cmplo <= 0 && cmphi >= 0) queue.enqueue(x.key); 
        if (cmphi > 0) keys(x.derecha, queue, lo, hi); 
    }
    
    private void values(Nodo<K,V> x, Queue<V> queue, K lo, K hi) { 
        if (x == null) return; 
        int cmplo = lo.compareTo(x.key); 
        int cmphi = hi.compareTo(x.key); 
        if (cmplo < 0) values(x.izquierda, queue, lo, hi); 
        if (cmplo <= 0 && cmphi >= 0) queue.enqueue(x.value); 
        if (cmphi > 0) values(x.derecha, queue, lo, hi); 
    }

	@Override
	public Iterator<V> valuesInRange(K init, K end) {
		Queue<V> values = new Queue<>();
		values(raiz, values, init, end);
		return values.iterator();
	}

	@Override
	public Iterator<K> keysInRange(K init, K end) {
		return keys(init, end);
	}

}
