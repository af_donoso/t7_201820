package api;

import data_structures.List;
import vo.VOBike;
import vo.VOTrip;

public interface IManager {
	void load();
	VOBike consultarBicicleta(int id);
	List<Integer> consultarIds(int idMenor, int idMayor);
}
