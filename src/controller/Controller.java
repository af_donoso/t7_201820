package controller;

import data_structures.List;
import logic.Manager;
import vo.VOBike;
import vo.VOTrip;

public class Controller {
	
	private static Manager manager = new Manager();
	
	public static void load() {
		long start = System.currentTimeMillis();
		manager.load();
		long end = System.currentTimeMillis();
		long time = end - start;
		System.out.println(time);
	}
	
	public static VOBike consultarBicicleta(int id) {
		return manager.consultarBicicleta(id);
	}

	public static List<Integer> consultarIds(int idMenor, int idMayor) {
		return manager.consultarIds(idMenor, idMayor);
	}
}
