package t7_201820;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Iterator;

import org.junit.jupiter.api.Test;

import data_structures.Nodo;
import data_structures.RedBlackBST;

class RedBlackBSTTest {

	class Prueba {
		int id;
		String cadena;

		Prueba(int id, String cadena) {
			this.id = id;
			this.cadena = cadena;
		}

		/* (non-Javadoc)
		 * @see java.lang.Object#equals(java.lang.Object)
		 */
		@Override
		public boolean equals(Object obj) {
			Prueba prueba = (Prueba) obj;
			return prueba.id == this.id && prueba.cadena.equals(this.cadena);
		}


	}

	//------------------------------------------------------
	//Colores
	//------------------------------------------------------

	RedBlackBST<Integer, Prueba> arbol;

	//------------------------------------------------------
	//Scenarios
	//------------------------------------------------------

	/**
	 * Arbol vacio.
	 */
	void scenario1() {
		arbol = new RedBlackBST<>();
	}

	/**
	 * Arbol sin nodos hermanos.
	 */
	void scenario2() {
		arbol = new RedBlackBST<>();

		for(int i = 1; i <= 7; i++) {
			Prueba prueba = new Prueba(i, "cadena" + i);
			arbol.put(prueba.id, prueba);
		}
	}

	/**
	 * Casos:
	 * 1. Un nodo. Raiz debe tener enlace negro.
	 * 2. Arbol sin nodos hermanos. Todos los enlaces deben ser negros.
	 * 3. Arbol con nodos hermanos. Algunos enlaces son rojos.
	 * 		3.1 Enlace rojo derecha.
	 * 		3.2 Enlace rojo izquierda.
	 * 		3.3 Enlace rojo derecha e izquierda.
	 */
	@Test
	void testPut() {
		//Caso 1
		scenario1();
		Prueba prueba = new Prueba(1, "String1");
		arbol.put(prueba.id, prueba);
		assertFalse(arbol.buscarNodo(prueba.id).esEnlaceRojo());
		assertEquals(1, arbol.size(), "El peso no corresponde.");
		assertEquals(0, arbol.height(), "La altura no corresponde.");

		//Caso 2
		scenario2();
		for(int i = 1; i <= 7; i++) {
			Nodo<Integer, Prueba> nodo = arbol.buscarNodo(i);

			if(nodo.esEnlaceRojo()) {
				fail("No deberían haber enlaces rojos.");
			}
		}
		assertEquals(7, arbol.size(), "El peso no corresponde.");
		assertEquals(2, arbol.height(), "La altura no corresponde.");

		//Caso 3.1
		scenario1();
		Prueba prueba1 = new Prueba(1, "cadena1");
		Prueba prueba2 = new Prueba(2, "cadena2");
		arbol.put(prueba1.id, prueba1);
		arbol.put(prueba2.id, prueba2);

		//Probar que se cambió correctamente la raiz.
		assertEquals(arbol.darRaiz().darValor(), prueba2, "La raíz debería tener la llave 2");
		assertTrue(arbol.buscarNodo(1).esEnlaceRojo(), "El enlace debería ser rojo.");

		//Caso 3.2
		scenario1();
		arbol.put(3, new Prueba(3, "cadena3"));
		arbol.put(2, new Prueba(2, "cadena2"));
		assertTrue(arbol.buscarNodo(2).esEnlaceRojo(), "El enlace debería ser rojo");

		//Caso 3.3
		arbol.put(4, new Prueba(4, "cadena4"));
		assertFalse(arbol.buscarNodo(2).esEnlaceRojo(), "El enlace debería ser negro");
		assertFalse(arbol.buscarNodo(4).esEnlaceRojo(), "El enlace debería ser negro");
	}

	/**
	 * Casos:
	 * 1. Arbol vacio.
	 * 2. Arbol con objetos.
	 */
	@Test
	void testGet() {
		//Caso 1
		scenario1();
		assertNull(arbol.get(1), "El objeto no debería existir.");

		//Caso 2
		scenario2();
		try {
			assertEquals(2, arbol.get(2).id, "El objeto retornado no corresponde.");
		} catch (NullPointerException e) {
			fail("El objeto debería existir.");
		}
	}

	@Test
	void testMin() {
		scenario2();
		assertEquals(Integer.valueOf(1), arbol.min(), "El mínimo no corresponde.");
	}

	@Test
	void testMax() {
		scenario2();
		assertEquals(Integer.valueOf(7), arbol.max(), "El máximo no corresponde.");
	}

	/**
	 * Casos:
	 * 1. El arbol esta vacio.
	 * 2. El arbol tiene objetos.
	 */
	@Test
	void testSize() {
		//Caso 1
		scenario1();
		assertEquals(0, arbol.size(), "El peso no corresponde.");

		//Caso 2
		scenario2();
		assertEquals(7, arbol.size(), "El peso no corresponde.");
	}

	/**
	 * Casos:
	 * 1. El arbol esta vacio.
	 * 2. El arbol tiene objetos.
	 * 		2.1 El arbol no tiene nodos hermanos.
	 * 		2.2 El arbol tiene nodos hermanos.
	 */
	@Test
	void testHeight() {
		//Caso 1
		scenario1();
		assertEquals(0, arbol.height(), "La altura no corresponde.");

		//Caso 2.1
		scenario2();
		assertEquals(2, arbol.height(), "La altura no corresponde.");

		//Caso 2.2
		arbol.put(8, new Prueba(8, "cadena8"));
		assertEquals(2, arbol.height(), "La altura no corresponde.");
	}

	/**
	 * Casos:
	 * 1. Arbol vacio.
	 * 2. Arbol con elementos.
	 */
	@Test
	void testIsEmpty() {
		//Caso 1
		scenario1();
		assertTrue(arbol.isEmpty(), "El árbol debería estar vacío.");

		//Caso 2
		scenario2();
		assertFalse(arbol.isEmpty(), "El árbol debería tener elementos.");
	}

	/**
	 * Casos:
	 * 1. Arbol vacio.
	 * 2. Arbol con elementos.
	 */
	@Test
	void testContains() {
		//Caso 1
		scenario1();
		assertFalse(arbol.contains(0), "El objeto no debería existir.");

		//Caso 2
		scenario2();
		assertTrue(arbol.contains(1), "El objeto debería existir.");
		assertFalse(arbol.contains(9), "El objeto no debería existir.");
	}

	/**
	 * Casos:
	 * 1. Arbol sin enlaces rojos.
	 * 2. Arbol con enlaces rojos.
	 * 		2.1 Enlaces rojos izquierda consecutivos.
	 */
	@Test
	void testCheck() {
		//Caso 1
		scenario2();
		assertTrue(arbol.check(), "El arbol contiene errores.");
		
		//Caso 2
		arbol.put(0, new Prueba(0, "cadena0"));
		arbol.put(8, new Prueba(8, "cadena8"));
		arbol.put(9, new Prueba(9, "cadena9"));
		arbol.put(10, new Prueba(10, "cadena10"));
		assertTrue(arbol.check(), "El arbol contiene errores.");
		
		//Caso 2.1
		scenario2();
		arbol.put(9, new Prueba(9, "cadena8"));
		arbol.put(8, new Prueba(8, "cadena8"));
		assertTrue(arbol.check(), "El arbol contiene errores.");
		
	}

	@Test
	void testKeys() {
		scenario2();

		Iterator<Integer> iter = arbol.keys();
		int i = 1;
		while(iter.hasNext()) {
			int key = iter.next();

			if(key != i) {
				fail("Las llaves no se están recorriendo en el orden correcto.");
			}

			System.out.println(key);
			
			i++;
		}
	}

	@Test
	void testValuesInRange() {
		scenario2();

		Iterator<Prueba> iter = arbol.valuesInRange(2,6);
		int i = 2;
		while(iter.hasNext()) {
			Prueba prueba = iter.next();

			if(prueba.id != i) {
				fail("Las llaves no se están recorriendo en el orden correcto.");
			}

			if(prueba.id < 2 || prueba.id > 6) {
				fail("No se está recorriendo dentro del rango establecido.");
			}

			Prueba esperado = new Prueba(i, "cadena" + i); 
			assertEquals(esperado, prueba, "El objeto retornado no es el esperado.");

			i++;
		}
	}

	@Test
	void testKeysInRange() {
		scenario2();

		Iterator<Integer> iter = arbol.keysInRange(2,6);
		int i = 2;
		while(iter.hasNext()) {
			int prueba = iter.next();

			if(prueba != i) {
				fail("Las llaves no se están recorriendo en el orden correcto.");
			}

			if(prueba < 2 || prueba > 6) {
				fail("No se está recorriendo dentro del rango establecido.");
			}
			
			assertEquals(i, prueba, "El objeto retornado no es el esperado.");

			i++;
		}
	}

}
